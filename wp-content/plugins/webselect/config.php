<?php
$builder = new DI\ContainerBuilder();
$builder->ignorePhpDocErrors(true);

$container = $builder->build();

foreach ($namespaces as $namespace) {
    $container->get($namespace);
}