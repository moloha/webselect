<?php
namespace Webselect\Elementor\ElementorWidgets\Widgets;

use Webselect\Elementor\ElementorWidgets\ElementorWidget;


class Title extends ElementorWidget
{

    protected $classes;

    function __construct($data = [], $args = null)
    {
        $this->classes = $args;

        $this->setName('Title Default');

        //@ToDo this not working
        $this->setTemplate('elementorTemplates/title-default');

        parent::__construct($data, $args);
    }


    protected function _register_controls()
    {

        $this->addTab('Title Default', function () {

            $this->textField('Title', 'title', 'Magento platform and we can help you today');

            $this->textareaField('Description', 'description', 'We support and encourage the thirst for innovative ideas. Our client’s growth success is our top priority');

            $this->chooseField('Position', 'position', [
                'left' => [
                    'title' => 'left',
                    'icon' => 'fa fa-align-left',
                ],
                'center' => [
                    'title' => 'center',
                    'icon' => 'fa fa-align-center',
                ],
                'right' => [
                    'title' => 'right',
                    'icon' => 'fa fa-align-right',
                ],
            ], 'center');
        });
    }
}
