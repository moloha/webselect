<?php /* Template Name: Home */ ?>

<?php get_header();?>

<?php
$banner = get_field("banner");
?>

<section class="banner" >
     <div class="banner-inner fade">
         <div class="banner-img">
             <img src="<?=$banner['image1']?>">
         </div>
         <div class="banner-content ">
             <h4><?=$banner['up_title1']?></h4>
             <h1><?=$banner['title1']?></h1>
             <div><?=$banner['sub_title1']?></div>
         </div>
    </div>

    <div class="banner-inner fade">
        <div class="banner-img">
            <img src="<?=$banner['image2']?>">
        </div>
        <div class="banner-content">
            <h4><?=$banner['up_title2']?></h4>
            <h1><?=$banner['title2']?></h1>
            <div><?=$banner['sub_title2']?></div>
        </div>
    </div>

</section>




<?php
echo do_shortcode('[rev_slider alias="slider-1"][/rev_slider]');
?>

<?php the_content(); ?>

<?php get_footer();?>
