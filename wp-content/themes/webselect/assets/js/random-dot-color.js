jQuery(document).ready(function ($) {
    let existPositions = [
        '#00D47A',
        '#F57907',
        '#C8CDFD',
        '#01E3E2',
        '#DF6EAE',
        '#255AB6',
        '#479CFF'
    ];

    shuffleArray(existPositions);

    const selector = $('.dot');

    selector.each(function (index) {
        $(this).css('background-color', existPositions[index]);
    });
});