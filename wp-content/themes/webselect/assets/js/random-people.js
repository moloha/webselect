jQuery(document).ready(function ($) {
    let existPositions = [
        '-10px -10px',
        '-398px -10px',
        '-10px -398px',
        '-398px -398px',
        '-786px -10px',
        '-786px -398px'
    ];

    shuffleArray(existPositions);

    const selector = $('.people');

    selector.each(function (index) {
        $(this).css('background-position', existPositions[index]);
        $(this).css('opacity', '1');
    });
});