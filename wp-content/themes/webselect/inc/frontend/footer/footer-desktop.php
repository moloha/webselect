<div class="row">
    <div class="container-big">
        <div class="header-content">
            <div class="logo">
                <img src="<?= getOptionField('header_logo'); ?>" alt="<?= get_the_title(); ?>"/>
            </div>
            <div class="right-side">
                <div class="main-menu">
                    <?php wp_nav_menu(array(
                        'theme_location' => 'header_menu'
                    )); ?>
                </div>
                <div class="contact-button">
                    <a class="button-secondary" href="<?= getOptionField('contact_us_button')['url']; ?>"
                       target="<?= getOptionField('contact_us_button')['target']; ?>">
                        <?= getOptionField('contact_us_button')['title']; ?>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>